package com.fortech.sports.restModel;

import java.util.Date;
import java.util.Set;

public class BookingResponse {

	public BookingResponse() {

	}
	private Long id;
	
	private Date startTime;

	private Date endTime;

	private PersonResponse createdBy;

	private Set<PersonResponse> atendees;

	private FieldResponse field;

	private Double price;

	public Date getStartTime() {
		return startTime;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public PersonResponse getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(PersonResponse createdBy) {
		this.createdBy = createdBy;
	}

	public Set<PersonResponse> getAtendees() {
		return atendees;
	}

	public void setAtendees(Set<PersonResponse> atendees) {
		this.atendees = atendees;
	}

	public FieldResponse getField() {
		return field;
	}

	public void setField(FieldResponse field) {
		this.field = field;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


}
