package com.fortech.sports.restModel;

import com.fortech.sports.model.Person;

public class AuthenticationResponse {

	private String autenticationToken;
	private Person person;

	public AuthenticationResponse(String autenticationToken,Person person) {
		this.autenticationToken = autenticationToken;
		this.person=person;
	}

	public String getAutenticationToken() {
		return autenticationToken;
	}

	public void setAutenticationToken(String autenticationToken) {
		this.autenticationToken = autenticationToken;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
	
		
	
}
