package com.fortech.sports.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "field")
@AttributeOverride(name = "id", column = @Column(name = "idField"))
public class Field extends AbstractEntity<Long> {

	@Column
	@NotNull(message = "Field name can not be null!")
	@Size(min = 3, message = "Field name should contain at least 3 characters!")
	private String name;

	@Column
	@NotNull(message = "Field address can not be null!")
	private String address;

	@Column
	@NotNull(message = "Field capacity can not be null!")
	@Min(value = 0L, message = "The capacity value must be a positive value!")
	private Integer capacity;

	@Column
	@Enumerated(EnumType.STRING)
	@NotNull(message = "Field type can not be null!")
	private SportType sportType;

	@Column
	private String details;

	public Field() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public SportType getSportType() {
		return sportType;
	}

	public void setSportType(SportType sportType) {
		this.sportType = sportType;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

}
