package com.fortech.sports.model;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "price")
@AttributeOverride(name = "id", column = @Column(name = "idPrice"))
public class Price extends AbstractEntity<Long> {

	@Column
	@NotNull(message = "Price can not be null!")
	@Min(value = 0L, message = "The price value must be a positive value!")
	private Double price;

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull(message = "Start time can not be null!")
	private Date startTime;

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull(message = "End time can not be null!")
	private Date endTime;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "field_id")
	private Field field;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

}
