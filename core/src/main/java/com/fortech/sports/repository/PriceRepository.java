package com.fortech.sports.repository;

import java.util.Set;

import com.fortech.sports.abstractRepository.BaseRepository;
import com.fortech.sports.model.*;

public interface PriceRepository extends BaseRepository<Price, Long> {

	Set<Price> findByField(Long fieldId);

}
