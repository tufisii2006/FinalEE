package com.fortech.sports.repository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.fortech.sports.abstractRepository.BaseRepositoryBean;
import com.fortech.sports.model.Person;

@Stateless
public class PersonRepositoryImpl extends BaseRepositoryBean<Person, Long> implements PersonRepository {

	@PersistenceContext(name = "Sports")
	private EntityManager entityManager;

	public PersonRepositoryImpl() {
		super(Person.class);
	}

	@Override
	public Person findByEmail(String email) {
		TypedQuery<Person> findBySportType = entityManager.createNamedQuery("Person.findByEmail", Person.class)
				.setParameter("email", email);
		Person person = (Person) findBySportType.getSingleResult();
		return person;
	}

	@Override
	public Person findByUsernameAndPassword(String username, String password) {
		TypedQuery<Person> findByUsernameAndPassword = entityManager
				.createNamedQuery("Person.findByUsernameAndPassword", Person.class).setParameter("username", username)
				.setParameter("password", password).setMaxResults(1);
		return findByUsernameAndPassword.getResultList().stream().findFirst().orElse(null);
	}

	@Override
	public Person findByUsername(String username) {
		TypedQuery<Person> findByUsername = entityManager.createNamedQuery("Person.findByUsername", Person.class)
				.setParameter("username", username).setMaxResults(1);
		return findByUsername.getResultList().stream().findFirst().orElse(null);

	}

}
