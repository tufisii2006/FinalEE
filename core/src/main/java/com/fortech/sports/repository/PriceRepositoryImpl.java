package com.fortech.sports.repository;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.fortech.sports.abstractRepository.BaseRepositoryBean;
import com.fortech.sports.model.*;

@Stateless
public class PriceRepositoryImpl extends BaseRepositoryBean<Price, Long> implements PriceRepository {

	@PersistenceContext(name = "Sports")
	private EntityManager entityManager;

	public PriceRepositoryImpl() {
		super(Price.class);
	}

	@Override
	public Set<Price> findByField(Long fieldId) {
		TypedQuery<Price> findByField = entityManager.createNamedQuery("Price.findByFieldId", Price.class)
				.setParameter("fieldId", fieldId);
		Set<Price> pricesByField = new HashSet<Price>(findByField.getResultList());
		return pricesByField;
	}

}
