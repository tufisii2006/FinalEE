package com.fortech.sports.repository;

import com.fortech.sports.abstractRepository.BaseRepository;
import com.fortech.sports.model.Person;

/**
 * Performs DataBase operation methods for {@link Person} Entity
 *
 */

public interface PersonRepository extends BaseRepository<Person, Long> {

	/**
	 * Retrieve a {@link Person} entity from the DataBase using its email
	 * 
	 * @param email - The email of the {@link Person}
	 * @return {@link Person}
	 */
	Person findByEmail(String email);

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	Person findByUsernameAndPassword(String username, String password);

	Person findByUsername(String username);
}
