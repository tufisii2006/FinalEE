package com.fortech.sports.customExceptions;

public class InvalidProvidedArgumentException extends Exception{

	private static final long serialVersionUID = -825016939030567713L;

	public InvalidProvidedArgumentException(String message) {
		super(message); 
	}

}
