package com.fortech.sports.customExceptions;

public class UsernameAlreadyExistException extends Exception {

	private static final long serialVersionUID = 789333830918223641L;

	public UsernameAlreadyExistException(String message) {
		super(message);
	}
}
