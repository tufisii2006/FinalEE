package com.fortech.sports.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Used for getting the start and end time of a certain day date
 * 
 * @author radu.tufisi
 *
 */
public class BookingDateUtils {

	/**
	 * Return a Date having the time stamp at 8 in the morning
	 * 
	 * @param date - the string date with the format "yyyy-MM-dd" that will be
	 *             converted to {@link Date} at 8 in the morning
	 * @return {@link Date}
	 * @throws ParseException - if the format is invalid
	 */
	public static Date getTheMorningTimeOfGivenDay(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dateObj = sdf.parse(date);
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(dateObj);
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		calendar.set(Calendar.MILLISECOND, 00);
		Date startDate = calendar.getTime();
		return startDate;
	}

	/**
	 * Return a Date having the time stamp at 23:59 midnight
	 * 
	 * @param date -he string date with the format "yyyy-MM-dd" that will be
	 *             converted to {@link Date} at 8 in the morning
	 * @return {@link Date}
	 * @throws ParseException - if the format is invalid
	 */
	public static Date getTheEndOfTheGivenDay(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dateObj = sdf.parse(date);
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(dateObj);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 59);
		Date endDate = calendar.getTime();
		return endDate;
	}

	public static Date getOnlyTheTimeOfAGivenDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(2000, 1, 1);
		Date returnDate = calendar.getTime();
		return returnDate;
	}

	public static Integer getHoursDifferenceBetweenTwoDates(Date end, Date start) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(start);
		calendar.setTimeZone(TimeZone.getTimeZone("Europe/Bucharest"));
		int aux1 = calendar.get(Calendar.HOUR_OF_DAY);
		System.out.println("aux1?: " + aux1);
		if (calendar.get(Calendar.MINUTE) >= 45) {
			aux1++;
		}
		calendar.setTime(end);
		int aux2 = calendar.get(Calendar.HOUR_OF_DAY);
		calendar.setTimeZone(TimeZone.getTimeZone("Europe/Bucharest"));
		System.out.println("aux2?: " + aux2);
		if (calendar.get(Calendar.MINUTE) >= 45) {
			aux2++;
		}
		return Math.abs(aux2 - aux1);
	}

}
