package com.fortech.sports.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordEncryption {

	public static String get_SHA_512_SecurePassword(String passwordToHash) {
		String generatedPassword = null;
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
			byte[] bytes = messageDigest.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				stringBuilder.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			generatedPassword = stringBuilder.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}

	public void caca() {
		String originalPassword = "password";
		String generatedSecuredPasswordHash = BCryptPassword.hashpw(originalPassword, BCryptPassword.gensalt(12));
		System.out.println(generatedSecuredPasswordHash);

		boolean matched = BCryptPassword.checkpw(originalPassword, generatedSecuredPasswordHash);
		System.out.println(matched);
	}

}
