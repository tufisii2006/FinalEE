package com.fortech.sports.security;

import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Stateless;

import com.fortech.sports.model.Person;

@Stateless
public class AdvancedSecurityContext {

	private Map<String, Person> sessionUsers = new TreeMap<String, Person>();

	public void addAuthenticatedUser(String key, Person value) {
		sessionUsers.put(key, value);
	}

	public void removeAuthenticatedUser(String key) {
		sessionUsers.remove(key);
	}

	public Map<String, Person> getSessionUsers() {
		return sessionUsers;
	}

	public void setSessionUsers(Map<String, Person> sessionUsers) {
		this.sessionUsers = sessionUsers;
	}
	
	
}
