package com.fortech.sports.security;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.fortech.sports.mappers.PersonMapper;
import com.fortech.sports.model.Person;
import com.fortech.sports.restModel.AuthenticationRequest;
import com.fortech.sports.restModel.AuthenticationResponse;
import com.fortech.sports.restModel.PersonResponse;
import com.fortech.sports.restModel.ResponseMessageEntity;

@Path("/")
@Stateless
public class AuthenticatorRESTendpoints {

	@Context
	private HttpServletRequest request;

	@EJB
	private Authenticator authenticator;

	@POST
	@Path("login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(AuthenticationRequest loginRequest) {
		try {
			AuthenticationResponse authResponse = authenticator.login(loginRequest.getUsername(), loginRequest.getPassword()) ;
			NewCookie sessionCookie = authenticator.createDomainCookie(authResponse.getAutenticationToken(), 10);		
			return Response.status(Status.OK).entity(authResponse).cookie(sessionCookie).build();
		} catch (LoginException e) {
			return Response.status(Status.CONFLICT).entity(e.getMessage()).build();
		}
	}

	@GET
	@Path("logout")
	@Produces(MediaType.APPLICATION_JSON)
	public Response logout() {
		try {
			ResponseMessageEntity response = new ResponseMessageEntity("Success Log-out!", Status.OK);
			NewCookie sessionCookie = new NewCookie("SESSION", "", "/", null, NewCookie.DEFAULT_VERSION, null, 1, null,
					false, false);
			return Response.status(Status.OK).entity(response).cookie(sessionCookie).build();
		} catch (NullPointerException | UnsupportedOperationException e) {
			ResponseMessageEntity response = new ResponseMessageEntity(e.getMessage(), Status.CONFLICT);
			return Response.status(Status.CONFLICT).entity(response).build();
		}
	}
	
	@GET
	@Path("sessionUser")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSessionUser(@CookieParam(value = "SESSION") Cookie cookie) {
		PersonResponse personResponse = new PersonResponse();
		try {
			Person personToBeInserted = authenticator.getAuthorizationTokensStorage().get(cookie.getValue());
			personResponse = PersonMapper.mapEntityToResponse(personToBeInserted);
			return Response.ok(personResponse, MediaType.APPLICATION_JSON).build();			
		} catch (NullPointerException | UnsupportedOperationException e) {
			ResponseMessageEntity response = new ResponseMessageEntity(e.getMessage(), Status.CONFLICT);
			return Response.status(Status.CONFLICT).entity(response).build();
		}
	}

}
