package com.fortech.sports.security;

import java.util.HashMap;

import java.util.Map;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.security.auth.login.LoginException;
import javax.ws.rs.core.NewCookie;

import org.apache.commons.lang3.StringUtils;

import com.fortech.sports.customExceptions.NoSuchEntryException;
import com.fortech.sports.customExceptions.NullArgumentException;
import com.fortech.sports.customExceptions.RepositoryException;
import com.fortech.sports.model.Person;
import com.fortech.sports.restModel.AuthenticationResponse;
import com.fortech.sports.service.PersonService;
import com.fortech.sports.utils.PasswordEncryption;

@Stateless
public class Authenticator {

	@EJB
	private PersonService personService;

	@EJB
	private AdvancedSecurityContext securityContext;

	private Map<String, Person> authorizationTokensStorage = new HashMap<String, Person>();

	public Authenticator() {

	}

	public AuthenticationResponse login(String username, String password) throws LoginException {
		if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
			throw new LoginException("Please enter both username and password!");
		}
		try {
			password = PasswordEncryption.get_SHA_512_SecurePassword(password);
			Person person = personService.getPersonByUsernameAndPassword(username, password);
			String authToken = UUID.randomUUID().toString() + UUID.randomUUID().toString();
			authorizationTokensStorage.entrySet().removeIf(p -> p.getValue().getId() == person.getId());
			authorizationTokensStorage.put(authToken, person);
			securityContext.getSessionUsers().entrySet().removeIf(p -> p.getValue().getId() == person.getId());
			securityContext.addAuthenticatedUser(authToken, person);
			AuthenticationResponse resp= new AuthenticationResponse(authToken, person);
			return resp;
		} catch (NullArgumentException | RepositoryException e) {
			throw new LoginException(e.getMessage());
		} catch (NoSuchEntryException e) {
			throw new LoginException("Invalid login credentials!");
		}
	}

	public boolean isAuthTokenValid(String token) {
		if (authorizationTokensStorage.containsKey(token)) {
			return true;
		}
		return false;
	}

	public Map<String, Person> getAuthorizationTokensStorage() {
		return authorizationTokensStorage;
	}

	public void setAuthorizationTokensStorage(Map<String, Person> authorizationTokensStorage) {
		this.authorizationTokensStorage = authorizationTokensStorage;
	}

	public NewCookie createDomainCookie(String value, int maxAgeInMinutes) {
		NewCookie newCookie = new NewCookie("SESSION", value, "/", null,NewCookie.DEFAULT_VERSION,
				null, maxAgeInMinutes * 60, null, false, false);
		return newCookie;
	}
}
