package com.fortech.sports.mappers;

import org.modelmapper.ModelMapper;

import com.fortech.sports.model.Person;
import com.fortech.sports.restModel.PersonRequest;
import com.fortech.sports.restModel.PersonResponse;

/**
 * Performs the mapping of {@link PersonRequest} or {@link PersonResponse} and
 * {@link Person} entity
 **/
public class PersonMapper {

	/**
	 * Maps a {@link Person} to a {@link PersonResponse}
	 * 
	 * @param person
	 *            - the {@link Person} to be mapped
	 * @return {@link PersonResponse}
	 */
	public static PersonResponse mapEntityToResponse(Person person) {
		ModelMapper modelMapper = new ModelMapper();
		PersonResponse personResponse = modelMapper.map(person, PersonResponse.class);
		return personResponse;
	}

	/**
	 * Maps a {@link PersonRequest} to a {@link Person}
	 * 
	 * @param personRequest
	 *            - the person request to be converted to {@link Person}
	 * @return {@link Person}
	 */
	public static Person mapRequestToEntity(PersonRequest personRequest) {
		ModelMapper modelMapper = new ModelMapper();
		Person person = modelMapper.map(personRequest, Person.class);
		return person;
	}

}
