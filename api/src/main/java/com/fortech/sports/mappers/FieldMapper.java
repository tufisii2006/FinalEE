package com.fortech.sports.mappers;

import org.modelmapper.ModelMapper;

import com.fortech.sports.model.Field;
import com.fortech.sports.restModel.FieldRequest;
import com.fortech.sports.restModel.FieldResponse;

/**
 * Performs the mapping of {@link FieldRequest} or {@link FieldResponse} and
 * {@link Field} entity
 *
 */
public class FieldMapper {

	/**
	 * Maps a {@link Field} to a{@link FieldResponse}
	 *
	 * @param field
	 *            - the {@link Field} to be converted
	 * @return {@link FieldResponse}
	 */
	public static FieldResponse mapEntityToResponse(Field field) {
		ModelMapper modelMapper = new ModelMapper();
		FieldResponse fieldResponse = modelMapper.map(field, FieldResponse.class);
		return fieldResponse;
	}

	/**
	 * Maps a {@link FieldRequest} to a {@link Field}
	 * 
	 * @param fieldRequest
	 *            - the {@link FieldRequest} that will be converted to a
	 *            {@link Field}
	 * @return {@link Field}
	 */
	public static Field mapRequestToEntity(FieldRequest fieldRequest) {
		ModelMapper modelMapper = new ModelMapper();
		Field field = modelMapper.map(fieldRequest, Field.class);
		return field;
	}

}
