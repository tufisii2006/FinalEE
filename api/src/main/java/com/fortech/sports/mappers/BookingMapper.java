package com.fortech.sports.mappers;

import org.modelmapper.ModelMapper;

import com.fortech.sports.model.Booking;
import com.fortech.sports.restModel.BookingAddRequest;
import com.fortech.sports.restModel.BookingResponse;

/**
 * Performs the mapping of {@link BookingAddRequest} or {@link BookingResponse} and
 * {@link Booking} entity
 * 
 * @author radu.tufisi
 *
 */
public class BookingMapper {

	/**
	 * Maps a {@link Booking} to a{@link BookingResponse}
	 *
	 * @param booking
	 *            - the {@link Booking} to be converted
	 * @return {@link BookingAddRequest}
	 */
	public static BookingResponse mapEntityToResponse(Booking booking) {
		ModelMapper modelMapper = new ModelMapper();
		BookingResponse bookingResponse = modelMapper.map(booking, BookingResponse.class);
		return bookingResponse;
	}

	/**
	 * Maps a {@link BookingAddRequest} to a {@link Booking}
	 * 
	 * @param bookingRequest
	 *            - the {@link BookingAddRequest} that will be converted to a
	 *            {@link Booking}
	 * @return {@link Booking}
	 */
	public static Booking mapRequestToEntity(BookingAddRequest bookingRequest) {
		ModelMapper modelMapper = new ModelMapper();
		Booking booking = modelMapper.map(bookingRequest, Booking.class);
		booking.setCreatedBy(PersonMapper.mapRequestToEntity(bookingRequest.getCreatedBy()));
		return booking;
	}

}
