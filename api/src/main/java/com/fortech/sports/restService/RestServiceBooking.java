package com.fortech.sports.restService;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import com.fortech.sports.customExceptions.InvalidProvidedArgumentException;
import com.fortech.sports.customExceptions.NullArgumentException;
import com.fortech.sports.customExceptions.RepositoryException;
import com.fortech.sports.customExceptions.NoSuchEntryException;
import com.fortech.sports.mappers.BookingMapper;
import com.fortech.sports.model.Booking;
import com.fortech.sports.model.Field;
import com.fortech.sports.model.Person;
import com.fortech.sports.restConfig.Secured;
import com.fortech.sports.restModel.BookingAddRequest;
import com.fortech.sports.restModel.BookingResponse;
import com.fortech.sports.restModel.PersonRequest;
import com.fortech.sports.restModel.BookingDeleteRequest;
import com.fortech.sports.restModel.ResponseMessageEntity;
import com.fortech.sports.security.AdvancedSecurityContext;
import com.fortech.sports.security.Authenticator;
import com.fortech.sports.service.BookingService;
import com.fortech.sports.service.PersonService;
import com.fortech.sports.utils.BookingDateUtils;

/**
 * REST interface for {@link Booking} entity
 *
 */
@Path("/booking")
@Stateless
public class RestServiceBooking {

	@EJB
	private BookingService bookingService;

	@EJB
	private PersonService personService;

	@EJB
	private Authenticator authenticator;

	@Context
	private HttpServletRequest request;

	@EJB
	private AdvancedSecurityContext securityContext;

	/**
	 * 
	 * @param personId
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/allBookings/{personId}")
	public Response getAllBookingsWhereAPersonIsParticipant(@PathParam("personId") Long personId) {
		Set<Booking> allBookings = new HashSet<Booking>();
		Set<BookingResponse> responseBookings = new HashSet<BookingResponse>();
		try {
			allBookings = bookingService.getAllBookingsOfAPersonWhereHeParticipate(personId);
			allBookings.stream().forEach(booking -> responseBookings.add(BookingMapper.mapEntityToResponse(booking)));
			return Response.ok(responseBookings, MediaType.APPLICATION_JSON).build();
		} catch (NullArgumentException | NoSuchEntryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}
	}

	/**
	 * 
	 * @param fieldId
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/allBookings/field/{fieldId}")
	public Response getAllBookingsMadeOnACertainField(@PathParam("fieldId") Long fieldId) {
		Set<Booking> allBookings = new HashSet<Booking>();
		Set<BookingResponse> responseBookings = new HashSet<BookingResponse>();
		try {
			allBookings = bookingService.getAllBookingsByAField(fieldId);
			allBookings.stream().forEach(booking -> responseBookings.add(BookingMapper.mapEntityToResponse(booking)));
			return Response.ok(responseBookings, MediaType.APPLICATION_JSON).build();
		} catch (NullArgumentException | RepositoryException | NoSuchEntryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}
	}

	/**
	 * Get all available booking on a fields in one day starting from 00:00 to 23:59
	 * 
	 * @param fieldId - the id of the {@link Field}
	 * @param date    - the {@link Date}
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/allBookings/{date}/{fieldId}")
	public Response getAllAvailableBookingsSortedByFieldAndStartDateAndEndDate(@PathParam("fieldId") Long fieldId,
			@PathParam("date") String date) {
		Set<Booking> allBookings = new HashSet<Booking>();
		Set<BookingResponse> responseBookings = new HashSet<BookingResponse>();
		try {
			allBookings = bookingService.getAllAvailableBookingsSortedByFieldAndStartDateAndEndDate(fieldId,
					BookingDateUtils.getTheMorningTimeOfGivenDay(date), BookingDateUtils.getTheEndOfTheGivenDay(date));
			allBookings.stream().forEach(booking -> responseBookings.add(BookingMapper.mapEntityToResponse(booking)));
			return Response.ok(responseBookings, MediaType.APPLICATION_JSON).build();
		} catch (NullArgumentException | ParseException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}

	}

	/**
	 * Retrieve all the bookings from the DataBase
	 * 
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/all")
	public Response getAllBookings(@Context SecurityContext sc) {
		Set<Booking> allBookings = new HashSet<Booking>();
		Set<BookingResponse> responseBookings = new HashSet<BookingResponse>();
		try {
			allBookings = bookingService.getAllBooking();
			allBookings.stream().forEach(booking -> responseBookings.add(BookingMapper.mapEntityToResponse(booking)));
			return Response.ok(responseBookings, MediaType.APPLICATION_JSON).build();
		} catch (RepositoryException | NoSuchEntryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}

	}

	/**
	 * Retrieve all {@link Booking} of a {@link Person}
	 * 
	 * @param id - the id of the {@link Person}
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/all/person/{id}")
	public Response getAllBookingsByPerson(@PathParam("id") Long id) {
		Set<Booking> allBookingsOfCertainPerson = new HashSet<Booking>();
		Set<BookingResponse> responseBookings = new HashSet<BookingResponse>();
		try {
			allBookingsOfCertainPerson = bookingService.getAllBookingOfGivenPerson(id);
			allBookingsOfCertainPerson.stream()
					.forEach(booking -> responseBookings.add(BookingMapper.mapEntityToResponse(booking)));
			return Response.ok(responseBookings, MediaType.APPLICATION_JSON).build();
		} catch (RepositoryException | NoSuchEntryException | NullArgumentException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();

		}

	}

	/**
	 * Retrieve a {@link Booking} from the DataBase
	 * 
	 * @param id - the id of the {@link Booking}
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/all/{id}")
	public Response getBookingById(@PathParam("id") Long id) {
		Booking booking = new Booking();
		BookingResponse responseBooking = new BookingResponse();
		try {
			booking = bookingService.getOneBookingById(id);
			responseBooking = BookingMapper.mapEntityToResponse(booking);
			return Response.ok(responseBooking, MediaType.APPLICATION_JSON).build();
		} catch (RepositoryException | NullArgumentException | NoSuchEntryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}

	}

	/**
	 * Creates a new {@link Booking} in the DataBase
	 * 
	 * @param bookingRequest - Containing the {@link Booking} details,
	 *                       {@link Person} owner of the booking and the
	 *                       {@link Field}
	 * @return {@link Response} -response containing data and HTTP Status code
	 * @throws IOException
	 */

	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Secured
	public Response createNewBooking(BookingAddRequest bookingRequest, @CookieParam("SESSION") Cookie cookie)
			throws IOException {
		Booking booking = new Booking();
		BookingResponse response = new BookingResponse();
		try {
			Long ownerId = securityContext.getSessionUsers().get(cookie.getValue()).getId();
			bookingRequest.setCreatedBy(new PersonRequest());
			booking = BookingMapper.mapRequestToEntity(bookingRequest);
			bookingService.createBooking(booking, ownerId, bookingRequest.getField().getId());
			response = BookingMapper.mapEntityToResponse(booking);
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (NullArgumentException | RepositoryException | InvalidProvidedArgumentException
				| NoSuchEntryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.CONFLICT);
			return Response.status(Status.CONFLICT).entity(exceptionResponse).build();
		}
	}

	/**
	 * Add a new {@link Person} to a existing {@link Booking}
	 * 
	 * @param bookingRequest
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@PATCH
	@Path("/addPerson")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addPersonToExistingBooking(BookingDeleteRequest bookingRequest) {
		Booking booking = new Booking();
		BookingResponse response = new BookingResponse();
		try {
			booking = bookingService.getOneBookingById(bookingRequest.getBookingId());
			Person person = personService.getOnePersonById(bookingRequest.getPersonId());
			bookingService.addPersonToExistingBooking(bookingRequest.getBookingId(), person);
			response = BookingMapper.mapEntityToResponse(booking);
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (NullArgumentException | RepositoryException | NoSuchEntryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.CONFLICT);
			return Response.status(Status.CONFLICT).entity(exceptionResponse).build();
		}

	}

	/**
	 * Remove a {@link Person} from and exiting {@link Booking}
	 * 
	 * @param bookingRequest
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@PATCH
	@Path("/removePerson")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removePersonFromExistingBooking(BookingDeleteRequest bookingRequest) {
		try {
			bookingService.removePersonFromExistingBooking(bookingRequest.getBookingId(), bookingRequest.getPersonId());
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity("Success!", Status.OK);
			return Response.status(Status.OK).entity(exceptionResponse).build();
		} catch (NullArgumentException | RepositoryException | NoSuchEntryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.CONFLICT);
			return Response.status(Status.CONFLICT).entity(exceptionResponse).build();
		}

	}

	/**
	 * Delete an existing {@link Booking} from the DataBase
	 * 
	 * @param bookingRequest
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@DELETE
	@Path("/removeBooking")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeBooking(BookingDeleteRequest bookingRequest) {
		try {
			bookingService.deleteBooking(bookingRequest.getBookingId());
			ResponseMessageEntity responseMessage = new ResponseMessageEntity("Success!", Status.OK);
			return Response.status(Status.OK).entity(responseMessage).build();
		} catch (NullArgumentException | RepositoryException | NoSuchEntryException e) {
			ResponseMessageEntity responseMessage = new ResponseMessageEntity(e.getMessage(), Status.CONFLICT);
			return Response.status(Status.CONFLICT).entity(responseMessage).build();
		}

	}

	/**
	 * Retrieve a set of {@link Booking} between time intervals
	 * 
	 * @param bookingRequest
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@POST
	@Path("/getInfo/bookingsOnTimeInterval")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllBookingsBetweenTwoDates(BookingAddRequest bookingRequest) {
		Set<Booking> booking = new HashSet<Booking>();
		try {
			booking = bookingService.getAllBookingsBetweenTimeIntervals(bookingRequest.getStartTime(),
					bookingRequest.getEndTime());
			return Response.ok(booking, MediaType.APPLICATION_JSON).build();
		} catch (NullArgumentException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}
	}

}
