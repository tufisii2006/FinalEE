package com.fortech.sports.restService;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.fortech.sports.customExceptions.NoSuchEntryException;
import com.fortech.sports.customExceptions.NullArgumentException;
import com.fortech.sports.customExceptions.RepositoryException;
import com.fortech.sports.customExceptions.UsernameAlreadyExistException;
import com.fortech.sports.mappers.PersonMapper;
import com.fortech.sports.model.Person;
import com.fortech.sports.restModel.PersonRequest;
import com.fortech.sports.restModel.PersonResponse;
import com.fortech.sports.restModel.ResponseMessageEntity;
import com.fortech.sports.service.BookingService;
import com.fortech.sports.service.PersonService;
import com.fortech.sports.utils.EmailUtil;

/**
 * REST interface for {@link Person} entity
 *
 */
@Path("/person")
@Stateless
public class RestServicePerson {

	@EJB
	private PersonService personService;

	@EJB
	private BookingService bookingService;

	/**
	 * Return a list of all available users from the DataBase
	 * 
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/all")
	public Response getAllUsers() {
		Set<Person> allUsersFromDB = new HashSet<Person>();
		Set<PersonResponse> responseUsers = new HashSet<PersonResponse>();
		try {
			allUsersFromDB = personService.getAllPersons();
			allUsersFromDB.stream().forEach(user -> responseUsers.add(PersonMapper.mapEntityToResponse(user)));
			return Response.ok(responseUsers, MediaType.APPLICATION_JSON).build();
		} catch (RepositoryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}

	}

	/**
	 * Delete a {@link Person} entry from the DataBase
	 * 
	 * @param personRequest - the {@link Person} that will be deleted
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@DELETE
	@Path("/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeUser(PersonRequest personRequest) {
		PersonResponse responseUser = new PersonResponse();
		try {
			personService.deletePerson(personRequest.getId());
			return Response.ok(responseUser, MediaType.APPLICATION_JSON).build();
		} catch (RepositoryException | NullArgumentException | NoSuchEntryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}
	}

	/**
	 * Insert a new {@link Person} in the DataBase
	 * 
	 * @param personRequest - The person that will be inserted in the DataBase
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerUser(PersonRequest personRequest) {
		Person personToBeInserted = PersonMapper.mapRequestToEntity(personRequest);
		PersonResponse personResponse = new PersonResponse();
		try {
			personService.addPerson(personToBeInserted);
			personResponse = PersonMapper.mapEntityToResponse(personToBeInserted);
			return Response.ok(personResponse, MediaType.APPLICATION_JSON).build();
		} catch (NullArgumentException | RepositoryException | UsernameAlreadyExistException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.CONFLICT);
			return Response.status(Status.CONFLICT).entity(exceptionResponse).build();
		}

	}

	/**
	 * Return a list of {@link PersonResponse} representing all the persons enrolled
	 * to a booking
	 * 
	 * @param id - the id of the booking
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/allPersonsOfCertainBooking/{id}")
	public Response getAllUsersOfSpecificBooking(@PathParam("id") Long id) {
		Set<Person> allUsersFromDB = new HashSet<Person>();
		Set<PersonResponse> responseUsers = new HashSet<PersonResponse>();
		try {
			allUsersFromDB = bookingService.getOneBookingById(id).getAtendees();
			allUsersFromDB.stream().forEach(user -> responseUsers.add(PersonMapper.mapEntityToResponse(user)));
			return Response.ok(responseUsers, MediaType.APPLICATION_JSON).build();
		} catch (NullArgumentException | RepositoryException | NoSuchEntryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}
	}

	/**
	 * 
	 * @param idBooking
	 * @param email
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@GET
	@Path("/joinMyBooking/{idBooking}/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response confirmRezervation(@PathParam("idBooking") Long idBooking, @PathParam("email") String email) {
		try {
			Person person = personService.getOnePersonByEmail(email);
			bookingService.addPersonToExistingBooking(idBooking, person);
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity("Success!", Status.OK);
			return Response.status(Status.OK).entity(exceptionResponse).build();
		} catch (NullArgumentException | RepositoryException | NoSuchEntryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}
	}

	/**
	 * 
	 * @param idBooking
	 * @param email
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@GET
	@Path("/sendInvitation/{idBooking}/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addPersonToExistingBookingByEmailInvitation(@PathParam("idBooking") Long idBooking,
			@PathParam("email") String email) {
		try {
			EmailUtil.sendMail(email, "Participa la rezervarea mea!",
					"Acceseaza acest link pentru a da join la rezervarea mea ! \\r\\n " +

							"    localhost:8080/api/person/joinMyBooking/" + idBooking + "/" + email);
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity("Success!", Status.OK);
			return Response.status(Status.OK).entity(exceptionResponse).build();
		} catch (MessagingException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}
	}

}
