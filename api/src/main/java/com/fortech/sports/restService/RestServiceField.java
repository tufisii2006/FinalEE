package com.fortech.sports.restService;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.fortech.sports.customExceptions.NullArgumentException;
import com.fortech.sports.customExceptions.RepositoryException;
import com.fortech.sports.mappers.FieldMapper;
import com.fortech.sports.model.Field;
import com.fortech.sports.model.SportType;
import com.fortech.sports.restModel.*;
import com.fortech.sports.service.BookingService;
import com.fortech.sports.service.FieldService;

/**
 * REST interface for {@link Field} entity
 *
 */
@Path("/field")
@Stateless
public class RestServiceField {

	@EJB
	private FieldService fieldService;

	@EJB
	private BookingService bookingService;

	/**
	 * Return all {@link Field} having specific {@link SportType}
	 * 
	 * @param type - The sport type
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/type/{type}")
	public Response getAllFieldsOfSpecificType(@PathParam("type") SportType type) {
		Set<Field> allFieldsOfSpecificType = new HashSet<Field>();
		Set<FieldResponse> responseFields = new HashSet<FieldResponse>();
		try {
			allFieldsOfSpecificType = fieldService.getAllFieldsOfGivenSportType(type);
			allFieldsOfSpecificType.stream()
					.forEach(field -> responseFields.add(FieldMapper.mapEntityToResponse(field)));
			return Response.ok(responseFields, MediaType.APPLICATION_JSON).build();
		} catch (RepositoryException | NullArgumentException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}

	}

	/**
	 * Get all {@link Field} between a period of time
	 * 
	 * @param request - the request containing start time and end time
	 * @return {@link Response} -response containing data and HTTP Status code
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/timeInterval/")
	public Response getAllFieldsBetweenTimeIntervals(FieldSearchRequest request) {
		Set<Field> allFields = new HashSet<Field>();
		try {
			allFields = fieldService.getAllFieldsAvailableBetweenTimeIntervals(request.getStartTime(),
					request.getEndTime());
			return Response.ok(allFields, MediaType.APPLICATION_JSON).build();
		} catch (NullArgumentException | RepositoryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/all")
	public Response getAllFieldse() {
		Set<Field> allFieldsOfSpecificType = new HashSet<Field>();
		Set<FieldResponse> responseFields = new HashSet<FieldResponse>();
		try {
			allFieldsOfSpecificType = fieldService.getAllFields();
			allFieldsOfSpecificType.stream()
					.forEach(field -> responseFields.add(FieldMapper.mapEntityToResponse(field)));
			return Response.ok(responseFields, MediaType.APPLICATION_JSON).build();
		} catch (RepositoryException e) {
			ResponseMessageEntity exceptionResponse = new ResponseMessageEntity(e.getMessage(), Status.NOT_FOUND);
			return Response.status(Status.NOT_FOUND).entity(exceptionResponse).build();
		}

	}

}
