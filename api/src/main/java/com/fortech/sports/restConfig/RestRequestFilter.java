package com.fortech.sports.restConfig;

import java.io.IOException;


import javax.annotation.Priority;
import javax.ejb.EJB;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;

import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;

import com.fortech.sports.security.AdvancedSecurityContext;
import com.fortech.sports.security.Authenticator;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
/*
 * By default a single instance of each provider class is instantiated for each
 * JAX-RS application
 */
public class RestRequestFilter implements ContainerRequestFilter {

	@EJB
	private Authenticator authenticator;

	@EJB
	private AdvancedSecurityContext securityContext;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		Cookie sessionCookie = requestContext.getCookies().get("SESSION");
		if (sessionCookie == null || StringUtils.isEmpty(sessionCookie.getValue())) {
			requestContext.abortWith(
					Response.status(Response.Status.FORBIDDEN).entity("No cookie no booking!!").build());
			return;
		}
		if (!authenticator.isAuthTokenValid(sessionCookie.getValue())) {
			requestContext.abortWith(
					Response.status(Response.Status.FORBIDDEN).entity("Session expired! Please log in again!").build());
			return;
		}
		String auToken = sessionCookie.getValue();
		if (securityContext.getSessionUsers().get(auToken) == null) {
			Response.status(Response.Status.FORBIDDEN).entity("Session expired! Please log in again!").build();
			return;
		}
	}

}
