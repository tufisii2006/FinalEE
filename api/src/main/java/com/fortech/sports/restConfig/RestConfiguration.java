package com.fortech.sports.restConfig;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configure all the REST interfaces of the application. ApplicationPath is the
 * root path of all REST interfaces
 */
@ApplicationPath("/api")
public class RestConfiguration extends Application {

}
