package com.fortech.sports.service;

import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.fortech.sports.model.Price;
import com.fortech.sports.repository.PriceRepository;

@Stateless
public class PriceServiceImpl implements PriceService {

	@EJB
	private PriceRepository priceRepository;

	@Override
	public Set<Price> getAllPricesOfAField(Long fieldId) {
		return priceRepository.findByField(fieldId);
	}

}
