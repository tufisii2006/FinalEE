package com.fortech.sports.service;

import java.util.Set;

import com.fortech.sports.model.Price;

public interface PriceService {

	public Set<Price> getAllPricesOfAField(Long fieldId);

}
